# web4all

Firefox browser extension to help realize the weight and impact of the web.
Try browsing the web on a 50 Mb daily budget.
Why 50 Mb? https://www.smashingmagazine.com/2019/07/web-on-50mb-budget/